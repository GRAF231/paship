import 'dotenv/config'
import {Logs} from './logs'
import Express from 'express'
import bodyParser from 'body-parser'
import Mongoose from 'mongoose'
import {authMiddleware} from '@utils/auth'
import {UserModel} from '@models/user'

const PORT : number = +(process.env.PORT || 8080)
const MONGO : string = process.env.MONGO_URL || ''

const app = Express()

app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', req.get('origin'))
    res.header('Access-Control-Allow-Headers', '*')
    res.header('Access-Control-Allow-Methods', '*')
    // if (req.method === 'OPTIONS') return res.status(204).send()
    next()
})

app.use(bodyParser.json())
app.use(authMiddleware())

app.use((req, res, next) => {
    bodyParser.raw({
        type: 'image/png',
        limit: '5mb'
    })(req, res, err => err ? res.status(400).json({code: 'UPLOAD_FAILED', message: 'Ошибка при загрузке файла, возможно его размер превышает 5 мегабайт.'}) : next())
})
app.use((req, res, next) => {
    bodyParser.raw({
        type: 'image/jpeg',
        limit: '5mb'
    })(req, res, err => err ? res.status(400).json({code: 'UPLOAD_FAILED', message: 'Ошибка при загрузке файла, возможно его размер превышает 5 мегабайт.'}) : next())
})

app.use('/auth', require('./routes/auth.router').default)
app.use('/courses', require('./routes/courses.router').default)
app.use('/categories', require('./routes/categories.router').default)
app.use('/images', require('./routes/images.router').default)
app.use('/partners', require('./routes/partners.router').default)
app.use('/orders', require('./routes/orders.router').default)

app.use((req, res) => {
    res.status(404).json({code: 'NOT_FOUND', message: 'Запрашиваемый эндпоинт не найден или не поддерживает метод ' + req.method})
})

async function start() {
    try{
        Mongoose.Schema.Types.String.checkRequired(v => v != null)
        await Mongoose.connect(MONGO)
        app.listen(PORT, () => {
            console.log(`App has been started on port ${PORT}`)
        })
    }
    catch (e) {
        console.error('Start failed', e)
        process.exit(1)
    }
}

start().catch(err => Logs.crash(err))