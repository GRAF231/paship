import {UserType} from '@models/user'
import {Request, Response} from 'express'
import jwt from 'jsonwebtoken'

export interface AuthObjectInterface {
    sub: string
    role: UserType
}

export function authMiddleware() {
    return (req: Request, res: Response, next: Function) => {
        if(req.headers['authorization']){
            try{
                const decoded : any = jwt.verify(req.headers['authorization'], process.env.JWT_SECRET || '');
                if(decoded && typeof decoded == 'object') req.auth = decoded;
                next();
            }
            catch(err){
                req.auth = null;
                next();
            }
        }
        else{
            req.auth = null;
            next();
        }
    }
}

export function authorized() {
    return (req: Request, res: Response, next: Function) => {
        if (!req.auth) {
            res.status(403).json({code: 'AUTHORISATION_REQUIRED', message: 'Для выполнения данного действия требуется авторизация'});
        } else next()
    }
}

export function userType(allowedTypes: UserType[]) {
    return (req: Request, res: Response, next: Function) => {
        if (!allowedTypes.includes(req.auth!.role)) {
            res.status(403).json({code: 'AUTHORISATION_REQUIRED', message: 'Для выполнения данного действия требуется одна из следующих ролей: ' + allowedTypes.join(', ')});
        } else next()
    }
}

declare global {
    namespace Express {
        export interface Request {
            auth: AuthObjectInterface | null
        }
    }
}