import Express from 'express'
import {validationResult} from 'express-validator'

export function validate() {
    return (req: Express.Request, res: Express.Response, next: Function) => {
        const validation = validationResult(req)
        if(!validation.isEmpty())
            return res.status(400).json({code: `INVALID_${validation.array()[0].location?.toUpperCase()}_${validation.array()[0].param?.toUpperCase()}`, message: validation.array()[0].msg})
        next()
    }
}