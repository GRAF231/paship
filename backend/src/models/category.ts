import { Schema, model, Types } from 'mongoose'

export interface CategorySchema {
    title: string
    created: number
    updated: number
}

const schema = new Schema<CategorySchema>({
    title: { type: String, required: true },
    created: { type: Number, required: true },
    updated: { type: Number, required: true },
})

export const CategoryModel = model<CategorySchema>('Category', schema)
