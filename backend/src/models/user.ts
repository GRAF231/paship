import { Schema, model, Types } from 'mongoose'

export type UserType = 'admin' | 'partner'

export interface UserSchema {
    name: string
    email: string
    password: string
    type: UserType
    created: number
}

const schema = new Schema<UserSchema>({
    name: { type: String, required: true },
    email: { type: String, required: true },
    password: { type: String, required: true },
    type: { type: String, enum: ['admin', 'partner'] },
    created: { type: Number, required: true },
})

export const UserModel = model<UserSchema>('User', schema)
