import { Schema, model, Types } from 'mongoose'

export interface ImageSchema {
    content: Buffer
    type: string
    uploader: Types.ObjectId
    created: number
}

const schema = new Schema<ImageSchema>({
    content: { type: Buffer, required: true },
    type: { type: String, required: true },
    uploader: { type: Schema.Types.ObjectId, ref: 'User', required: true },
    created: { type: Number, required: true },
})

export const ImageModel = model<ImageSchema>('Image', schema)
