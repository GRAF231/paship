import { Schema, model, Types } from 'mongoose'

export interface CourseSchema {
    title: string
    description: string
    category?: Types.ObjectId
    url: string
    image?: Types.ObjectId
    price: number
    salePrice: number
    createdBy: Types.ObjectId
    created: number
    updated: number
}

const schema = new Schema<CourseSchema>({
    title: { type: String, required: true },
    description: { type: String, required: true },
    category: { type: Schema.Types.ObjectId, ref: 'Category', required: false },
    url: { type: String, required: true },
    image: { type: Schema.Types.ObjectId, ref: 'Image', required: false },
    price: { type: Number, required: true },
    salePrice: { type: Number, required: true },
    createdBy: { type: Schema.Types.ObjectId, ref: 'User', required: true },
    created: { type: Number, required: true },
    updated: { type: Number, required: true },
})

export const CourseModel = model<CourseSchema>('Course', schema)
