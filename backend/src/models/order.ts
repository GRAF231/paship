import { Schema, model, Types } from 'mongoose'

export interface OrderSchema {
    course: Types.ObjectId
    partner: Types.ObjectId
    created: number
}

const schema = new Schema<OrderSchema>({
    course: { type: Schema.Types.ObjectId, ref: 'Course', required: true },
    partner: { type: Schema.Types.ObjectId, ref: 'User', required: true },
    created: { type: Number, required: true },
})

export const OrderModel = model<OrderSchema>('Order', schema)
