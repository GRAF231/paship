import {Router} from 'express'
import handle from '@utils/errorHandler'
import {body, param} from 'express-validator'
import bcrypt from 'bcryptjs'
import {validate} from '@utils/validate'
import {UserModel} from '@models/user'
import jwt from 'jsonwebtoken'
import {AuthObjectInterface, authorized, userType} from '@utils/auth'
import {CourseModel} from '@models/course'
import {CategoryModel} from '@models/category'
import {Types} from 'mongoose'
import {OrderModel} from '@models/order'

const router = Router()

router.get('/', [
    authorized(),
    userType(['admin']),
], handle(async (req, res) => {
    const partners = await UserModel.find({ type: 'partner' })
    return res.json({partners: await Promise.all(partners.map(async el => ({
        id: el._id.toString(),
        name: el.name,
        email: el.email,
        sells: await OrderModel.countDocuments({ partner: el._id }),
        created: el.created,
    })))})
}))

router.get('/me', [
    authorized(),
    userType(['partner']),
], handle(async (req, res) => {
    const partner = await UserModel.findById(req.auth!.sub)
    return res.json({
        id: partner!._id.toString(),
        name: partner!.name,
        email: partner!.email,
        sells: await OrderModel.countDocuments({ partner: partner!._id }),
        created: partner!.created,
    })
}))

router.get('/:id/courses', [
    param('id').isMongoId(),
    validate(),
    authorized(),
    userType(['admin', 'partner']),
], handle(async (req, res) => {
    if (req.auth!.role !== 'admin' && req.params.id != req.auth!.sub) return res.status(403).json({code: 'AUTHORISATION_REQUIRED', message: 'Для выполнения данного действия требуется одна из следующих ролей: admin'})
    const id = new Types.ObjectId(req.params.id)
    const courses = await CourseModel.find()
    return res.json({courses: await Promise.all(courses.map(async el => ({
        id: el._id.toString(),
        title: el.title,
        description: el.description,
        category: el.category ? await (async () => {
            const category = await CategoryModel.findById(el.category)
            return {
                id: category!._id.toString(),
                title: category!.title,
                created: category!.created,
                updated: category!.updated,
            }
        })() : null,
        url: el.url,
        image: el.image?.toString() || null,
        price: el.price,
        sale_price: el.salePrice,
        createdBy: el.createdBy.toString(),
        created: el.created,
        updated: el.updated,
        sells: await OrderModel.countDocuments({ partner: id, course: el._id }),
    })))})
}))

router.post('/', [
    body('name').isString(),
    body('email').isEmail(),
    body('password').isLength({min: 8}),
    validate(),
    authorized(),
    userType(['admin']),
], handle(async (req, res) => {
    const partner = await new UserModel({
        name: req.body.name,
        email: req.body.email,
        password: req.body.password,
        type: 'partner',
        created: Math.round(Date.now() / 1000)
    }).save()
    res.json({ id: partner._id.toString() })
}))

router.delete('/:id', [
    param('id').isMongoId(),
    validate(),
    authorized(),
    userType(['admin']),
], handle(async (req, res) => {
    await UserModel.deleteOne({ _id: new Types.ObjectId(req.params.id) })
    res.status(204).send()
}))

export default router