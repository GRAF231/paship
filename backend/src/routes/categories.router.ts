import {Router} from 'express'
import handle from '@utils/errorHandler'
import {body} from 'express-validator'
import bcrypt from 'bcryptjs'
import {validate} from '@utils/validate'
import {UserModel} from '@models/user'
import jwt from 'jsonwebtoken'
import {AuthObjectInterface, authorized, userType} from '@utils/auth'
import {CourseModel} from '@models/course'
import {CategoryModel} from '@models/category'

const router = Router()

router.get('/', [
    authorized(),
    userType(['admin', 'partner']),
], handle(async (req, res) => {
    const categories = await CategoryModel.find()
    return res.json({categories: await Promise.all(categories.map(async el => ({
        id: el._id.toString(),
        title: el.title,
        created: el.created,
        updated: el.updated,
    })))})
}))

export default router