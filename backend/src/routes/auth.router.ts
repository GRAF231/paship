import {Router} from 'express'
import handle from '@utils/errorHandler'
import {body} from 'express-validator'
import bcrypt from 'bcryptjs'
import {validate} from '@utils/validate'
import {UserModel} from '@models/user'
import jwt from 'jsonwebtoken'
import {AuthObjectInterface, authorized, userType} from '@utils/auth'

const router = Router()

router.post('/login', [
    body('email').isEmail(),
    body('password').isLength({min: 8}),
    validate(),
], handle(async (req, res) => {
    const user = await UserModel.findOne({email: req.body.email})
    if (!user) return res.status(401).json({code: 'INVALID_CREDENTIALS', message: 'Неправильный логин или пароль'})
    const passwordCorrect = await bcrypt.compare(req.body.password, user.password)
    if (!passwordCorrect) return res.status(401).json({code: 'INVALID_CREDENTIALS', message: 'Неправильный логин или пароль'})
    const authObject : AuthObjectInterface = {
        sub: user._id.toString(),
        role: user.type
    }
    const token = jwt.sign(
        authObject,
        process.env.JWT_SECRET || '',
        { expiresIn: '24h' }
    )
    res.json({ token, type: user.type })
}))

router.post('/register', [
    body('name').isString(),
    body('email').isEmail(),
    body('password').isLength({min: 8}),
    validate(),
], handle(async (req, res) => {
    if (await UserModel.findOne({email: req.body.email})) return res.status(400).json({code: 'EMAIL_USED', message: 'Адрес электронной почты уже используется другим пользователем'})
    const partner = await new UserModel({
        name: req.body.name,
        email: req.body.email,
        password: req.body.password,
        type: 'partner',
        created: Math.round(Date.now() / 1000)
    }).save()
    const authObject : AuthObjectInterface = {
        sub: partner._id.toString(),
        role: partner.type
    }
    const token = jwt.sign(
        authObject,
        process.env.JWT_SECRET || '',
        { expiresIn: '24h' }
    )
    res.json({ token, type: partner.type })
}))

export default router