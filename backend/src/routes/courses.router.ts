import {Router} from 'express'
import handle from '@utils/errorHandler'
import {body, param} from 'express-validator'
import bcrypt from 'bcryptjs'
import {validate} from '@utils/validate'
import {UserModel} from '@models/user'
import jwt from 'jsonwebtoken'
import {AuthObjectInterface, authorized, userType} from '@utils/auth'
import {CourseModel} from '@models/course'
import {CategoryModel} from '@models/category'
import {Types} from 'mongoose'

const router = Router()

router.get('/', [
    authorized(),
    userType(['admin', 'partner']),
], handle(async (req, res) => {
    const courses = await CourseModel.find()
    return res.json({courses: await Promise.all(courses.map(async el => ({
        id: el._id.toString(),
        title: el.title,
        description: el.description,
        category: el.category ? await (async () => {
            const category = await CategoryModel.findById(el.category)
            return {
                id: category!._id.toString(),
                title: category!.title,
                created: category!.created,
                updated: category!.updated,
            }
        })() : null,
        url: el.url,
        image: el.image?.toString() || null,
        price: el.price,
        sale_price: el.salePrice,
        createdBy: el.createdBy.toString(),
        created: el.created,
        updated: el.updated,
    })))})
}))

router.get('/:id', [
    param('id').isMongoId(),
    validate(),
    authorized(),
    userType(['admin', 'partner']),
], handle(async (req, res) => {
    const course = await CourseModel.findById(req.params.id)
    if (!course) return res.status(404).json({code: 'NOT_FOUND', message: 'Запрошенный ресурс не найден'})
    return res.json({
        id: course._id.toString(),
        title: course.title,
        description: course.description,
        category: course.category ? await (async () => {
            const category = await CategoryModel.findById(course.category)
            return {
                id: category!._id.toString(),
                title: category!.title,
                created: category!.created,
                updated: category!.updated,
            }
        })() : null,
        url: course.url,
        image: course.image?.toString() || null,
        price: course.price,
        sale_price: course.salePrice,
        createdBy: course.createdBy.toString(),
        created: course.created,
        updated: course.updated,
    })
}))

router.post('/', [
    body('title').isLength({min: 1}),
    body('description').isLength({min: 1}),
    body('url').isLength({min: 1}),
    body('category').isMongoId().optional({nullable: true}),
    body('image').isMongoId().optional({nullable: true}),
    body('price').isInt({min: 1}),
    body('sale_price').isInt({min: 1}),
    validate(),
    authorized(),
    userType(['admin']),
], handle(async (req, res) => {
    const course = await new CourseModel({
        title: req.body.title,
        description: req.body.description,
        url: req.body.url,
        category: req.body.category ? new Types.ObjectId(req.body.category) : undefined,
        image: req.body.image ? new Types.ObjectId(req.body.image) : undefined,
        price: +req.body.price,
        salePrice: +req.body.sale_price,
        createdBy: new Types.ObjectId(req.auth!.sub),
        created: Math.round(Date.now() / 1000),
        updated: Math.round(Date.now() / 1000),
    }).save()
    res.json({ id: course._id.toString() })
}))

router.patch('/:id', [
    param('id').isMongoId(),
    body('title').isLength({min: 1}),
    body('description').isLength({min: 1}),
    body('url').isLength({min: 1}),
    body('category').isMongoId().optional({nullable: true}),
    body('image').isMongoId().optional({nullable: true}),
    body('price').isInt({min: 1}),
    body('sale_price').isInt({min: 1}),
    validate(),
    authorized(),
    userType(['admin']),
], handle(async (req, res) => {
    await CourseModel.updateOne({ _id: new Types.ObjectId(req.params.id) }, {
        $set: {
            title: req.body.title,
            description: req.body.description,
            url: req.body.url,
            category: req.body.category ? new Types.ObjectId(req.body.category) : undefined,
            image: req.body.image ? new Types.ObjectId(req.body.image) : undefined,
            price: +req.body.price,
            salePrice: +req.body.sale_price,
            updated: Math.round(Date.now() / 1000)
        }
    })
    res.status(204).send()
}))

router.delete('/:id', [
    param('id').isMongoId(),
    validate(),
    authorized(),
    userType(['admin']),
], handle(async (req, res) => {
    await CourseModel.deleteOne({ _id: new Types.ObjectId(req.params.id) })
    res.status(204).send()
}))

export default router