import {Router} from 'express'
import handle from '@utils/errorHandler'
import {body, param} from 'express-validator'
import bcrypt from 'bcryptjs'
import {validate} from '@utils/validate'
import {UserModel} from '@models/user'
import jwt from 'jsonwebtoken'
import {AuthObjectInterface, authorized, userType} from '@utils/auth'
import {CourseModel} from '@models/course'
import {CategoryModel} from '@models/category'
import {ImageModel} from '@models/image'
import {fromBuffer} from 'file-type'
import {Types} from 'mongoose'

const router = Router()

router.get('/:id', [
    param('id').isMongoId(),
    validate(),
], handle(async (req, res) => {
    const image = await ImageModel.findById(req.params.id)
    if (!image) return res.status(404).send()
    res.type(image.type).send(image.content)
}))

router.post('/', [
    authorized(),
    userType(['admin']),
], handle(async (req, res) => {
    const buf = Buffer.from(req.body)
    const mimeInfo = await fromBuffer(buf)
    if(!mimeInfo || !['image/png', 'image/jpeg'].includes(mimeInfo.mime))
        return res.status(400).json({code: 'UNSUPPORTED_FILE_TYPE', message: 'Неподдерживаемый тип файла'})
    const image = await new ImageModel({
        type: mimeInfo.mime,
        created: Math.round(Date.now() / 1000),
        uploader: new Types.ObjectId(req.auth!.sub),
        content: buf
    }).save()
    res.json({ id: image._id.toString() })
}))

export default router