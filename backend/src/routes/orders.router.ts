import {Router} from 'express'
import handle from '@utils/errorHandler'
import {body, param} from 'express-validator'
import bcrypt from 'bcryptjs'
import {validate} from '@utils/validate'
import {UserModel} from '@models/user'
import jwt from 'jsonwebtoken'
import {AuthObjectInterface, authorized, userType} from '@utils/auth'
import {CourseModel} from '@models/course'
import {CategoryModel} from '@models/category'
import {Types} from 'mongoose'
import {OrderModel} from '@models/order'

const router = Router()

router.post('/', [
    body('partner').isMongoId(),
    body('course').isMongoId(),
    validate(),
], handle(async (req, res) => {
    await new OrderModel({
        partner: new Types.ObjectId(req.body.partner),
        course: new Types.ObjectId(req.body.course),
        created: Math.round(Date.now() / 1000),
    }).save()
    res.status(204).send()
}))

export default router